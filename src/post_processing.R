#Generate tables of performance stats #########################################
library('ProjectTemplate')
load.project()

#Dataframe objects
d.4000.mse.g <- d.4000.mse.f <-  d.4000.mse.l <- d.4000.mse.a <- 
  d.1000.mse.f <- d.1000.mse.l <- d.1000.mse.a <- d.500.mse.f <- 
  d.500.mse.l <- d.500.mse.a <- data.frame()

d.4000.mae.g <- d.4000.mae.f <-  d.4000.mae.l <- d.4000.mae.a <- 
  d.1000.mae.f <- d.1000.mae.l <- d.1000.mae.a <- d.500.mae.f <- 
  d.500.mae.l <- d.500.mae.a <- data.frame()

d.4000.mer.g <- d.4000.mer.f <-  d.4000.mer.l <- d.4000.mer.a <- 
  d.1000.mer.f <- d.1000.mer.l <- d.1000.mer.a <- d.500.mer.f <- 
  d.500.mer.l <- d.500.mer.a <- data.frame()


#Put results in dataframes ####
for (i in seq_len(nrep)){
  for(m in seq_len(length(method.list))){
    
    if(length(results[[i]])==3){
      
      ############################MSE##########################################
      #n=4000
      d.4000.mse.g[i,m] <- results[[i]][[1]][[m]][[3]][[2]] #nrep;samp=4000;method;model=gray;index=mse
      d.4000.mse.f[i,m] <- results[[i]][[1]][[m]][[1]][[2]]
      d.4000.mse.l[i,m] <- results[[i]][[1]][[m]][[2]][[2]]
      d.4000.mse.a[i,m] <- results[[i]][[1]][[m]][[4]][[2]]
      
      #n=1000
      d.1000.mse.f[i,m] <- results[[i]][[2]][[m]][[1]][[2]] #nrep;samp=1000;method;model=franks;index=mse
      d.1000.mse.l[i,m] <- results[[i]][[2]][[m]][[2]][[2]]
      d.1000.mse.a[i,m] <- results[[i]][[2]][[m]][[4]][[2]]
      
      #n=500
      d.500.mse.f[i,m] <- results[[i]][[3]][[m]][[1]][[2]] #nrep;samp=500;method;model=franks;index=mse
      d.500.mse.l[i,m] <- results[[i]][[3]][[m]][[2]][[2]]
      d.500.mse.a[i,m] <- results[[i]][[3]][[m]][[4]][[2]]
      
      ############################MAE##########################################
      #n=4000
      d.4000.mae.g[i,m] <- results[[i]][[1]][[m]][[3]][[1]] #nrep;samp=4000;method;model=gray;index=mae
      d.4000.mae.f[i,m] <- results[[i]][[1]][[m]][[1]][[1]]
      d.4000.mae.l[i,m] <- results[[i]][[1]][[m]][[2]][[1]]
      d.4000.mae.a[i,m] <- results[[i]][[1]][[m]][[4]][[1]]
      
      #n=1000
      d.1000.mae.f[i,m] <- results[[i]][[2]][[m]][[1]][[1]] #nrep;samp=1000;method;model=franks;index=mae
      d.1000.mae.l[i,m] <- results[[i]][[2]][[m]][[2]][[1]]
      d.1000.mae.a[i,m] <- results[[i]][[2]][[m]][[4]][[1]]
      
      #n=500
      d.500.mae.f[i,m] <- results[[i]][[3]][[m]][[1]][[1]] #nrep;samp=500;method;model=franks;index=mae
      d.500.mae.l[i,m] <- results[[i]][[3]][[m]][[2]][[1]]
      d.500.mae.a[i,m] <- results[[i]][[3]][[m]][[4]][[1]]
      
      ############################Mean error###################################
      #n=4000
      d.4000.mer.g[i,m] <- results[[i]][[1]][[m]][[3]][[3]] #nrep;samp=4000;method;model=gray;index=mer
      d.4000.mer.f[i,m] <- results[[i]][[1]][[m]][[1]][[3]]
      d.4000.mer.l[i,m] <- results[[i]][[1]][[m]][[2]][[3]]
      d.4000.mer.a[i,m] <- results[[i]][[1]][[m]][[4]][[3]]
      
      #n=1000
      d.1000.mer.f[i,m] <- results[[i]][[2]][[m]][[1]][[3]] #nrep;samp=1000;method;model=franks;index=mer
      d.1000.mer.l[i,m] <- results[[i]][[2]][[m]][[2]][[3]]
      d.1000.mer.a[i,m] <- results[[i]][[2]][[m]][[4]][[3]]
      
      #n=500
      d.500.mer.f[i,m] <- results[[i]][[3]][[m]][[1]][[3]] #nrep;samp=500;method;model=franks;index=mer
      d.500.mer.l[i,m] <- results[[i]][[3]][[m]][[2]][[3]]
      d.500.mer.a[i,m] <- results[[i]][[3]][[m]][[4]][[3]]
      
    } else { #There was an error caused by the gray model in the n=4000 sample size
      ############################MSE##########################################
      #Whole row is missing
      d.4000.mse.g[i,] <- NA
      d.4000.mse.f[i,] <- NA
      d.4000.mse.l[i,] <- NA
      d.4000.mse.a[i,] <- NA
      
      #n=1000
      d.1000.mse.f[i,m] <- results[[i]][[1]][[m]][[1]][[2]] #nrep;samp=1000;method;model=franks;index=mse
      d.1000.mse.l[i,m] <- results[[i]][[1]][[m]][[2]][[2]]
      d.1000.mse.a[i,m] <- results[[i]][[1]][[m]][[3]][[2]]
      
      #n=500
      d.500.mse.f[i,m] <- results[[i]][[2]][[m]][[1]][[2]] #nrep;samp=500;method;model=franks;index=mse
      d.500.mse.l[i,m] <- results[[i]][[2]][[m]][[2]][[2]]
      d.500.mse.a[i,m] <- results[[i]][[2]][[m]][[3]][[2]]
      
      ############################MAE##########################################
      #Whole row is missing
      d.4000.mae.g[i,] <- NA
      d.4000.mae.f[i,] <- NA
      d.4000.mae.l[i,] <- NA
      d.4000.mae.a[i,] <- NA
      
      #n=1000
      d.1000.mae.f[i,m] <- results[[i]][[1]][[m]][[1]][[1]] #nrep;samp=1000;method;model=franks;index=mae
      d.1000.mae.l[i,m] <- results[[i]][[1]][[m]][[2]][[1]]
      d.1000.mae.a[i,m] <- results[[i]][[1]][[m]][[3]][[1]]
      
      #n=500
      d.500.mae.f[i,m] <- results[[i]][[2]][[m]][[1]][[1]] #nrep;samp=500;method;model=franks;index=mae
      d.500.mae.l[i,m] <- results[[i]][[2]][[m]][[2]][[1]]
      d.500.mae.a[i,m] <- results[[i]][[2]][[m]][[3]][[1]]
      
      ############################Mean error###################################
      #Whole row is missing
      d.4000.mer.g[i,] <- NA
      d.4000.mer.f[i,] <- NA
      d.4000.mer.l[i,] <- NA
      d.4000.mer.a[i,] <- NA
      
      #n=1000
      d.1000.mer.f[i,m] <- results[[i]][[1]][[m]][[1]][[3]] #nrep;samp=1000;method;model=franks;index=mer
      d.1000.mer.l[i,m] <- results[[i]][[1]][[m]][[2]][[3]]
      d.1000.mer.a[i,m] <- results[[i]][[1]][[m]][[3]][[3]]
      
      #n=500
      d.500.mer.f[i,m] <- results[[i]][[2]][[m]][[1]][[3]] #nrep;samp=500;method;model=franks;index=mer
      d.500.mer.l[i,m] <- results[[i]][[2]][[m]][[2]][[3]]
      d.500.mer.a[i,m] <- results[[i]][[2]][[m]][[3]][[3]]
    }
  }
}

#Rename columns ####
colnames(d.4000.mse.g) <- 
  colnames(d.4000.mse.f) <-  
  colnames(d.4000.mse.l) <- 
  colnames(d.4000.mse.a) <- 
  colnames(d.1000.mse.f) <- 
  colnames(d.1000.mse.l) <- 
  colnames(d.1000.mse.a) <-
  colnames(d.500.mse.f) <- 
  colnames(d.500.mse.l) <- 
  colnames(d.500.mse.a) <- 
  colnames(d.4000.mae.g) <- 
  colnames(d.4000.mae.f) <-  
  colnames(d.4000.mae.l) <- 
  colnames(d.4000.mae.a) <- 
  colnames(d.1000.mae.f) <- 
  colnames(d.1000.mae.l) <- 
  colnames(d.1000.mae.a) <-
  colnames(d.500.mae.f) <- 
  colnames(d.500.mae.l) <- 
  colnames(d.500.mae.a) <- 
  colnames(d.4000.mer.g) <- 
  colnames(d.4000.mer.f) <-  
  colnames(d.4000.mer.l) <- 
  colnames(d.4000.mer.a) <- 
  colnames(d.1000.mer.f) <- 
  colnames(d.1000.mer.l) <- 
  colnames(d.1000.mer.a) <-
  colnames(d.500.mer.f) <- 
  colnames(d.500.mer.l) <- 
  colnames(d.500.mer.a) <- 
  method.list 

#Estimates of bias ####
# MSE
b.4000.mse.g <- my.bias(d.4000.mse.g)
b.4000.mse.f <- my.bias(d.4000.mse.f)
b.4000.mse.l <- my.bias(d.4000.mse.l)
b.4000.mse.a <- my.bias(d.4000.mse.a)

b.1000.mse.f <- my.bias(d.1000.mse.f)
b.1000.mse.l <- my.bias(d.1000.mse.l)
b.1000.mse.a <- my.bias(d.1000.mse.a)

b.500.mse.f <- my.bias(d.500.mse.f)
b.500.mse.l <- my.bias(d.500.mse.l)
b.500.mse.a <- my.bias(d.500.mse.a)

# MAE
b.4000.mae.g <- my.bias(d.4000.mae.g)
b.4000.mae.f <- my.bias(d.4000.mae.f)
b.4000.mae.l <- my.bias(d.4000.mae.l)
b.4000.mae.a <- my.bias(d.4000.mae.a)

b.1000.mae.f <- my.bias(d.1000.mae.f)
b.1000.mae.l <- my.bias(d.1000.mae.l)
b.1000.mae.a <- my.bias(d.1000.mae.a)

b.500.mae.f <- my.bias(d.500.mae.f)
b.500.mae.l <- my.bias(d.500.mae.l)
b.500.mae.a <- my.bias(d.500.mae.a)

# Mean error
b.4000.mer.g <- my.bias(d.4000.mer.g)
b.4000.mer.f <- my.bias(d.4000.mer.f)
b.4000.mer.l <- my.bias(d.4000.mer.l)
b.4000.mer.a <- my.bias(d.4000.mer.a)

b.1000.mer.f <- my.bias(d.1000.mer.f)
b.1000.mer.l <- my.bias(d.1000.mer.l)
b.1000.mer.a <- my.bias(d.1000.mer.a)

b.500.mer.f <- my.bias(d.500.mer.f)
b.500.mer.l <- my.bias(d.500.mer.l)
b.500.mer.a <- my.bias(d.500.mer.a)